// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import Header from './components/Header'
import Footer from './components/Footer'
import LimmobilierByGiteDeFrance from './components/LimmobilierByGiteDeFrance'
import Nav from './components/Nav'
import ObtenirLeLabel from './components/ObtenirLeLabel'
import Home from './components/Home'
import MaSelection from './components/MaSelection'
import DerniereMinute from './components/DerniereMinute'
import MonCompte from './components/MonCompte'
import QuiSommesNous from './components/QuiSommesNous'
import ContactezNous from './components/ContactezNous'
import MentionLegales from './components/MentionLegales'
import ProtectionDesDonnees from './components/ProtectionDesDonnees'
import NosPartenaires from './components/NosPartenaires'
import DecouvrirLeLot from './components/DecouvrirLeLot'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css'

Vue.use(VueAwesomeSwiper, /* { default global options } */)

Vue.use(VueRouter)

const router = new VueRouter ({
  mode: 'history',
  
  routes: [{
    path: '/',
    component: Home,
  }, {
    path: '/Header',
    component: Header,
  }, {
    path: '/MaSelection',
    component: MaSelection,
  }, {
    path: '/MonCompte',
    component: MonCompte,
  }, {
    path: '/Footer',
    component: Footer,
  }, {
    path: '/LimmobilierByGiteDeFrance',
    component: LimmobilierByGiteDeFrance,
  }, {
    path: '/Nav',
    component: Nav
  }, {
    path: '/ObtenirLeLabel',
    component: ObtenirLeLabel
  },  {
    path: '/DerniereMinute',
    component: DerniereMinute
  }, {
    path: '/QuiSommesNous',
    component: QuiSommesNous
  }, {
    path: '/ContactezNous',
    component: ContactezNous
  }, {
    path: '/MentionLegales',
    component: MentionLegales
  }, {
    path: '/ProtectionDesDonnees',
    component: ProtectionDesDonnees
  }, {
    path: '/NosPartenaires',
    component: NosPartenaires
  }, {
    path: '/DecouvrirLeLot',
    component: DecouvrirLeLot
  }, {
    path: '*',
    redirected: '/'
  }, {
    path: '/Home',
    component: Home
  }
  ],
scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  },
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
